package org.unioulu.lasertest;

public final class Helper
{
	
	public static byte[] intToBytes(int i)
	{
		byte [] ret = new byte[2];
		
		ret[0] = (byte) (i  & 0xffff);
		ret[1] = (byte) ((i >>> 8) & 0xff);
		return ret;
	}
	
	public static byte[] longToBytes(long i)
	{
		byte [] ret = new byte[4];
		
		ret[0] = (byte) (i  & 0xffff);
		ret[1] = (byte) ((i >>> 8) & 0xff);
		ret[2] = (byte) ((i >>> 16) & 0xff);
		ret[3] = (byte) ((i >>> 24) & 0xff);
		return ret;
	}
	
	public static int createXOR(byte b [], int len)
	{
		int XOR=0;
		for(int i=0;i<len;i++)
		{
			XOR ^= b[i];
		}
		
		return XOR;
	}
	
	public static int bytesToInt(byte a, byte b)
	{
		int ret = (int)a|b;
		return ret;
	}
	
	public static void clearByteBuffer(byte [] b)
	{
		for (int i=0;i<b.length;i++)
		{
			b[i] = (byte)0x0;
		}
	}
	
	public static byte [] concatBuffers (byte [] src, int srcStart, int srcEnd, byte[] dst)
	{
		int numbytes = srcEnd-srcStart;
		
		if (numbytes <0)
			return dst;
		
		
		
		if (dst != null)
		{
			byte [] ret = new byte[numbytes+dst.length];
			System.arraycopy(dst, 0, ret, 0, dst.length); //all of the destination
			System.arraycopy(src, srcStart, ret, dst.length, numbytes); //and the part from the source
			return ret;
		}
		else
		{
			byte [] ret = new byte[numbytes];
			System.arraycopy(src, srcStart, ret, 0, numbytes); //and the part from the source
			return ret;
		}
	}
	
	public static boolean match(byte []a ,byte [] b, int length)
	{
		for (int i=0;i<length;i++)
		{
			if (a[i] != b[i])
			{
				return false;
			}
		}
		
		return true;
	}
	
	public static int intmap(int x, int in_min, int in_max, int out_min, int out_max)
	{
	  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
	
}
