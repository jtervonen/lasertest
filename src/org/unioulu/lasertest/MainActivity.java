package org.unioulu.lasertest;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener,OnTouchListener{
	
	public static final String PIC_READY = "org.unioulu.lasertest.PIC_READY";
	public static final String CONNECTION = "org.unioulu.lasertest.CONNECTION";
	public static final String NO_CONNECTION = "org.unioulu.lasertest.NO_CONNECTION";
	
	private static final String sTag = "LaserTest";
	private ImageView mPicView; 
	private boolean mLogging = false;
	
	private final class Controller extends Thread
	{
		public Handler mHandler;
		private DataOutputStream output;
		private Socket s;
		private String addr;
		
		public Controller(String addr)
		{
			this.addr = addr;
		}
		public void run()
		{
			Looper.prepare();
			try {
				s = new Socket(addr, 9998);
			} catch (IOException e) {
				Intent i = new Intent();
				i.setAction(NO_CONNECTION);
				i.putExtra("sender","control");
				sendBroadcast(i);
				Looper.myLooper().quit();
				e.printStackTrace();
				return;
			}
			
			try
			{
				output = new DataOutputStream(s.getOutputStream());
			}catch(Exception e)
			{
			}
			mHandler = new Handler() {
				public void handleMessage(Message msg) {
					if (msg.getData().getBoolean("close", false))
					{
						try {
							output.close();
							s.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Looper.myLooper().quit();
						return;
					}
	                byte [] data = msg.getData().getByteArray("data");   
					try {
						output.write(data);
					} catch (Exception e) {
						Looper.myLooper().quit();
						return;
					}
	              }
	          };
	          
	          Looper.loop();
		}
		
	}
	
	private final class ImageReceiver
	{
		protected Socket socket;
		
		public ImageReceiver(Context c, String ip)
		{
			WorkerThread worker = new WorkerThread(c,ip);
			worker.start();
		}
		public void stop()
		{
			if (socket == null)
				return;
			synchronized(socket)
			{
				try
				{
					socket.close();
				}catch(Exception e)
				{}
			}
		}
		private final class WorkerThread extends Thread
		{
			DataInputStream isr;
			Context mContext;
			String addr;
			
			public WorkerThread (Context c, String ip)
			{
				mContext = c;
				this.addr = ip;
			}
			public void run()
			{
				Log.i(sTag,Thread.currentThread().getName()+" STARTING");
				try {
					socket = new Socket(addr, 9997);
				} catch (IOException e) {
					Intent i = new Intent();
					i.setAction(NO_CONNECTION);
					i.putExtra("sender","image");
					sendBroadcast(i);
					e.printStackTrace();
					return;
				}
				
				try
				{
					isr = new DataInputStream(socket.getInputStream());
				}catch(Exception e)
				{
					e.printStackTrace();
					try {
						if (socket != null)
							socket.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					return;
				}
				
				while(true)
				{
					try
					{
						if (isr.available()>0)
						{
							Bitmap map;
							synchronized(socket)
							{
								map = BitmapFactory.decodeStream(isr);
							}
							if (map != null)
								sendBcast(map);
						}
					}catch(IOException e)
					{
						//e.printStackTrace();
						Log.i(sTag,Thread.currentThread().getName()+" STOPPING");
						break;
					}
				}
			}
			
			private void sendBcast(Bitmap bm)
			{	
				Intent i = new Intent();
				i.setAction(PIC_READY);
				i.putExtra("bitmap", bm);
				sendBroadcast(i);
			}
		}
	}
	
	private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent i) {
			
			if (PIC_READY.equals(i.getAction())) {
				Bundle b = i.getExtras();
				Bitmap m = (Bitmap)b.get("bitmap");
				if (m != null)
				{
					if(mLogging)
						mActivityLogger.log(m);
					
					int width = m.getWidth();
			        int height = m.getHeight();
			        
			        int newWidth = 250;
			        int newHeight = 250;

			        float scaleWidth = ((float) newWidth) / width;
			        float scaleHeight = ((float) newHeight) / height;

			        Matrix matrix = new Matrix();
			        matrix.postScale(scaleWidth, scaleHeight);
			        matrix.postRotate(90);
			        
			        Bitmap rmap = Bitmap.createBitmap(m, 0, 0,
	                          width, height, matrix, true);
			        
	                m.recycle();
	                
					mPicView.setImageBitmap(rmap);
					mPicView.invalidate();
				}
			}
			else if (NO_CONNECTION.equals(i.getAction()))
			{
				Bundle b = i.getExtras();
				String s = (String)b.get("sender");
				if(b != null)
				{
					if ("image".equals(s))
					{
						mTextView.setText("Connection Failed");
					}
					else if ("control".equals(s))
					{
						mTextView.setText("Connection Failed");
					}
				}
			}
			else if (CONNECTION.equals(i.getAction()))
			{
				
			}
		}
	};
	
	
	private Context mContext;
	private String mServerIp = "";
	
	private ImageReceiver mReceiver = null;
	private Controller mController = null;
	private Point mPoint = new Point();
	private TextView mTextView;
	private int [] mButtons = {R.id.up_button,R.id.center_button,R.id.down_button,R.id.right_button,R.id.left_button};

	private WindowManager mWindowManager;
	private ActivityLogger mActivityLogger;
	
	private final OnSharedPreferenceChangeListener mOnSharedListener = new OnSharedPreferenceChangeListener(){

		public void onSharedPreferenceChanged(
				SharedPreferences prefs, String key) {
			if (key.equals("ip") && !mServerIp.equals(prefs.getString(key, "127.0.0.1")))
			{
				if (mReceiver != null)
		    	{
		    		mReceiver.stop();
		    		mReceiver = null;
		    	}
		    	if (mController != null)
		    	{
		    		Bundle b = new Bundle();
		    		b.putBoolean("close", true);
		    		Message m = Message.obtain();
		    		m.setData(b);
		    		mController.mHandler.sendMessage(m);
		    		mController = null;
		    	}
		    	String ip = prefs.getString(key, "127.0.0.1");
		    	mReceiver =  new ImageReceiver(mContext, ip);
		    	mController = new Controller(ip);
		    	mController.start();
			}
		}};
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mContext = this;
        setContentView(R.layout.activity_main);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        
        mPicView = (ImageView)findViewById(R.id.picview);
        mPicView.setOnTouchListener(this);
        mTextView = (TextView)findViewById(R.id.tview);
        
        for (int id : mButtons)
        {
        	Button b = (Button)findViewById(id);
        	b.setOnClickListener(this);
        }
        
        IntentFilter ifilter = new IntentFilter(); 
	    ifilter.addAction(PIC_READY);
	    ifilter.addAction(NO_CONNECTION);
	    registerReceiver(mIntentReceiver, ifilter);
	    
	    if(mLogging)
	    {
	    	mActivityLogger = new ActivityLogger(this);
	    	mActivityLogger.log(new Command("Create"));
	    }
	    
	    moveCenter();
    }

    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent i = new Intent();
                i.setClass(this, SettingsActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public void onDestroy()
    {
    	super.onDestroy();
    	if (mReceiver != null)
    	{
    		mReceiver.stop();
    		mReceiver = null;
    	}
    	if (mController != null)
    	{
    		Bundle b = new Bundle();
    		b.putBoolean("close", true);
    		Message m = Message.obtain();
    		m.setData(b);
    		mController.mHandler.sendMessage(m);
    		mController = null;
    	}
    	if (mActivityLogger != null)
    		mActivityLogger.close();
    }
    
    protected void onResume()
    {
    	Log.i(sTag, "on resume");
    	super.onResume();
    	
    	IntentFilter ifilter = new IntentFilter(); 
	    ifilter.addAction(PIC_READY);
    	registerReceiver(mIntentReceiver, ifilter);
    	
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    	String ip = prefs.getString("ip", "127.0.0.1");
    	if(!ip.equals(mServerIp))
    	{
    		if (mReceiver != null)
    			mReceiver.stop();
    		mReceiver =  new ImageReceiver(this, ip);
    	
    		if (mController != null && mController.mHandler != null)
    		{
    			Bundle b = new Bundle();
    			b.putBoolean("close", true);
    			Message m = Message.obtain();
    			m.setData(b);
    			mController.mHandler.sendMessage(m);
    			mController = null;
    		}
    	
    		mController = new Controller(ip);
    		mController.start();
    	}
    }
    
    protected void onPause()
    {
    	Log.i(sTag, "on pause");
    	super.onPause();
    	if (mReceiver != null)
    	{
    		mReceiver.stop();
    		mReceiver = null;
    	}
    	if (mController != null)
    	{
    		if (mController.mHandler != null)
    		{
    			Bundle b = new Bundle();
    			b.putBoolean("close", true);
    			Message m = Message.obtain();
    			m.setData(b);
    			mController.mHandler.sendMessage(m);
    		}
    		mController = null;
    	}
    	unregisterReceiver(mIntentReceiver);
    }

	public void onClick(View v) {
		int id = v.getId();
		
		switch(id)
		{
		case R.id.left_button:
			moveLeft();
			//sendPoint();
			break;
		case R.id.right_button:
			moveRight();
			//sendPoint();
			break;
		case R.id.up_button:
			moveUp();
			//sendPoint();
			break;
		case R.id.down_button:
			moveDown();
			//sendPoint();
			break;
		case R.id.center_button:
			moveCenter();
			sendPoint();
			break;
		default:
			break;
		}
		
	}
    
	private void moveLeft()
	{
		//mPoint.x = (mPoint.x > 0 ? mPoint.x-1 : 0);
		sendDirection(1);
	}
	private void moveRight()
	{
		//mPoint.x = (mPoint.x < 180 ? mPoint.x+1 : 180);
		sendDirection(2);
	}
	private void moveUp()
	{
		//mPoint.y = (mPoint.y < 180 ? mPoint.y+1 : 180);
		sendDirection(3);
	}
	private void moveDown()
	{
		//mPoint.y = (mPoint.y > 0 ? mPoint.y-1 : 0);
		sendDirection(4);
	}
	private void moveCenter()
	{
		mPoint.y = 90;
		mPoint.x = 90;
	}
	
	private void sendDirection(int dir)
	{
		if (mController == null || mController.mHandler == null)
			return;
		Message m = Message.obtain();
		Bundle b = new Bundle();
		byte [] data = new byte[5];
		data[0] = (byte)0x55;
		data[1] = (byte)(dir&0x00FF); 
		
		b.putByteArray("data", data);
		m.setData(b);
		if(mLogging)
			mActivityLogger.log(new Command("Direction:", data));
		mController.mHandler.sendMessage(m);
		
	}
	
	private void sendPoint()
	{
		mTextView.setText("X:Y="+Integer.valueOf(mPoint.x)+":"+Integer.valueOf(mPoint.y));
		if (mController == null || mController.mHandler == null)
			return;
		Message m = Message.obtain();
		Bundle b = new Bundle();
		byte [] data = new byte[5];
		data[0] = (byte)0x88;
		byte []	bytes = Helper.intToBytes(mPoint.x);
		System.arraycopy(bytes, 0, data, 1, 2);
		bytes = Helper.intToBytes(mPoint.y);
		System.arraycopy(bytes, 0, data, 3, 2);
		
		b.putByteArray("data", data);
		m.setData(b);
		
		if(mLogging)
			mActivityLogger.log(new Command("Point:", data));
		
		mController.mHandler.sendMessage(m);
	}

	public boolean onTouch(View v, MotionEvent event) {
		float x = event.getX(0);
		float y = event.getY(0);
		int h = v.getHeight();
		int w = v.getWidth();
		
		mPoint.x = Helper.intmap((int)x, w, 0, 130, 50);
		mPoint.y = Helper.intmap((int)y, h, 0, 160, 30);
		
		sendPoint();
		return false;
	}
}
