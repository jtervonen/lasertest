package org.unioulu.lasertest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.text.format.Time;

public class ActivityLogger {

	protected Vector<Object> logItems = new Vector<Object>();
	protected File logfile;
	protected FileOutputStream logStream;
	
	private final class LoggerThread extends Thread
	{
		private File imaged;
		private Time time;
		
		
		public LoggerThread(String name, File imagedir)
		{
			super(name);
			imaged = imagedir;
			this.time = new Time();
			
		}
		public void run()
		{
			while(true)
			{
				if (logItems.size() > 0)
				{
					Object o = logItems.remove(0);
					if (o instanceof Bitmap)
					{
						time.setToNow();
						File f = new File(imaged,time.format2445()+".png");
						FileOutputStream fos;
						try {
							fos = new FileOutputStream(f);
							((Bitmap) o).compress(Bitmap.CompressFormat.PNG, 100, fos);
							fos.close();
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException ioe)
						{
							ioe.printStackTrace();
						}
						
						StringBuilder sb = new StringBuilder("Image received: ");
						sb.append(time.format2445()+".png");
						
						try {
							logStream.write(sb.toString().getBytes());
						} catch (IOException e) {
							e.printStackTrace();
							break;
						}
					}
					
					else if( o instanceof Command)
					{
						try {
							logStream.write( ((Command)o).toString().getBytes() );
						} catch (IOException e) {
							e.printStackTrace();
							break;
						}
					}
				}
			}
		}
	}
	
	public ActivityLogger(Context context)
	{
		Time time = new Time();
		time.setToNow();
		File logfile = new File(context.getExternalFilesDir(null), time.format2445()+".log");
	    //File logfile = new File("/sdcard/lasertest/"+time.format2445()+".log");
	    //if (!logfile.mkdirs())
	    	//return;
	    
		/*try {
			logStream = context.openFileOutput(time.format2445()+".log",0);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	    try {
			logStream = new FileOutputStream(logfile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    //File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
	    File f = new File("/sdcard/lasertest/");
	    if (f.mkdirs())
	    {
	    	LoggerThread t = new LoggerThread("LoggerThread",f);
	    	t.start();
	    }
	}
	
	public void log(Bitmap m)
	{
		logItems.add(m);
	}
	
	public void log(Command c)
	{
		logItems.add(c);
	}
	
	public void close()
	{
		if (logStream != null)
		{
			try {
				logStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		logItems.add(new Command("STOP"));
	}
}