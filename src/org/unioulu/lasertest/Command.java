package org.unioulu.lasertest;

public class Command 
{
	private String type;
	private Object [] data;
	
	public Command(String type, Object [] data)
	{
		this.type = type;
		this.data = data;
	}
	
	public Command(String type)
	{
		this.type = type;
	}
	
	public Command(String type, byte [] data)
	{
		this.type = type;
		this.data = new Object[data.length];
		
		for (int i=0;i<data.length;i++)
		{
			this.data[i]=new Byte(data[i]);
		}
	}
	
	public String toString()
	{
		StringBuilder b = new StringBuilder();
		b.append(type);
		if (data != null)
		{
			for (Object o : data)
			{
				b.append(" ");
				b.append(o.toString());
			}
		}
		return b.toString();
	}
}
